<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);
require_once(dirname(__FILE__) . '/fontChecker.php');
require_once(dirname(__FILE__) . '/RGBcolors.php');



class PDFout
{
    public $data;
    public $page_width;
    public $page_height;
    public $page_horzmargin;
    public $page_vertmargin;
    public $page_ury;
    public $page_lly;
    public $page_no;
    public $template_path;
    public $content_width;
    public $pdflib_key;

    public function __construct($data, $page_width = 612, $page_height = 792, $page_horzmargin = 80, $page_vertmargin = 60)
    {
        $this->template_path = dirname(__FILE__) . "/templates/";
        $this->page_height = $page_height;
        $this->page_width = $page_width;
        $this->page_horzmargin = $page_horzmargin;
        $this->page_vertmargin = $page_vertmargin;
        $this->page_no = 1;
        $this->page_lly = $this->page_vertmargin;
        $this->page_ury = $page_height - $this->page_vertmargin;
        $this->content_width = $page_width - $page_horzmargin;
        $this->data = $data;
        //$this->pdflib_key = "L900202-019090-143865-PB3Z22-9ZYUC2";
        //$this->pdflib_key ="L900202-019070-140147-J7P382-2Z7D92";
    }

    function generatePDF()
    {

        $dataObj = $this->data;
        $format = $dataObj["format"];
        $content = json_decode($dataObj["json"], true);
        $author = $dataObj['author'];
        $template = $dataObj["template"];
        $pwd = "na";//$dataObj["pwd"];
        $save = $dataObj["save_opt"];
        $pageWidth = $this->page_width;
        $pageHeight = $this->page_height;
        $report_type = strtoupper($dataObj['report_type']);
        $heading = strtoupper($dataObj['heading']);
        $metadata = $dataObj['metadata'];
        $table_cols = explode(",",$dataObj['table_cols']);
        $table_cols_width = explode(",",$dataObj['table_cols_width']);
        $data_key = $dataObj['data_key'];
        $horz_margin = $this->page_horzmargin;

        $urx = $pageWidth - $horz_margin;

        $newline_re = '/\r\n|\n|\r/';
        $metadata = preg_replace($newline_re, $metadata, chr(10));

        $filename = isset($dataObj["filename"]) ? $dataObj["filename"] . ".pdf" : null;
        $passwords = $pwd ? " masterpassword=" . $pwd . chr(10) . " userpassword=periscope" : "";
        $encoding = 'unicode';
        $embedding = 'true';

        // page margins
        $horz_margin = $this->page_horzmargin;
        $vert_margin = $this->page_vertmargin;
        $content_width = $this->page_width - $horz_margin * 2;
        $content_height = $this->page_height - $vert_margin * 2;

        //NOTE: these fonts must exist inside /fonts/

        $font_body = "Roboto";
        $font_bold = "Roboto-Bold";
        $font_light = "Roboto-Light";
        $font_italic = "Roboto-Italic";
        $font_angelina = "Angelina";
        $bodyfs = 10;
        $h0fs = 20;
        $h1fs = ($h0fs * .9);
        $h2fs = ($h0fs * .8);
        $h3fs = ($h0fs * .7);
        $h4fs = ($h0fs * .6);
        $h5fs = ($h0fs * .5);
        $leading = "100%";

        $searchpath = dirname(__FILE__) . "/fonts";

        $p = new PDFlib();

        $fc = new FontChecker($p, $searchpath);
        $fc->getFonts();

        $p->set_option("errorpolicy=exception");
        $p->set_option("SearchPath={{" . $searchpath . "}}");
        $p->set_option("logging={filename=logs/debug.log remove}");
        //$p->set_option("license", $this->pdflib_key);


        // all strings are expected as utf8
        $p->set_option("stringformat=utf8");

        $docopts = " linearize=true
                    optimize=true
                    pagelayout=singlepage
                    viewerpreferences ={fitwindow=true}
                    openmode=bookmarks
                    " . $passwords;

        $tmpfile = $save ? "tmp.pdf" : "";
        $p->begin_document($tmpfile, $docopts);

        $p->set_info("Creator", "app.Periscope");
        $p->set_info("Author", $author);
        $p->set_info("Title", $report_type . " Report");


        $font = $p->load_font($font_body, "unicode", "embedding errorpolicy=return");
        $fontb = $p->load_font($font_bold, "unicode", "embedding errorpolicy=return");
        $fontl = $p->load_font($font_light, "unicode", "embedding errorpolicy=return");
        $fonti = $p->load_font($font_italic, "unicode", "embedding errorpolicy=return");
        $fonta = $p->load_font($font_angelina, "unicode", "embedding errorpolicy=return");

        $fontname = $p->get_string($p->info_font($font, "fontname", "api"), "");
        $fontnameb = $p->get_string($p->info_font($fontb, "fontname", "api"), "");
        $fontnamel = $p->get_string($p->info_font($fontl, "fontname", "api"), "");
        if ($font == 0) {
            print_r($p->get_errmsg());
            throw new Exception("Error: " . $p->get_errmsg());
        }

        $strokecolor = " fillcolor={rgb 0.0 0.0 0.0}";
        $strokecolor_red = " fillcolor={rgb 1.0 0.0 0.0}";
        $strokecolor_blue = " fillcolor={rgb 0.0 0.5 0.5}";

        // page numbering properties
        $pgno = $this->page_no;

        $titleOpts = "ruler={20 40 60 80} tabalignment={left left left left} " .
            "encoding=" .
            $encoding . " embedding=" .
            $embedding . " charref=true  fontname {" .
            $fontname . "} fontsize=" .
            $bodyfs . " hortabmethod=ruler  leading=" . $leading . " ";

        $headingOpts =
            "encoding=" .
            $encoding . " embedding=" .
            $embedding . " charref=true  fontname {" .
            $fontname . "} fontsize=" .
            $h3fs . " leading=" . $leading . " fakebold=true";

        $table_options = "fittextline {fontname {" . $fontname . "} encoding=unicode fontsize=" . $bodyfs . " embedding position {left top}}";

        if ($template != "blank") {
            $templatename = $this->template_path . $template;
            $p->begin_page_ext(0, 0, "width=" . $pageWidth . " height=" . $pageHeight);
            $stationery = $p->open_pdi_document($templatename, "");
            $page = $p->open_pdi_page($stationery, 1, "");
            $p->fit_pdi_page($page, 0, $pageHeight,
                "position={left top}");
            $p->close_pdi_page($page);
            $p->close_pdi_document($stationery);
        } else {
            $p->begin_page_ext(0, 0, "width=" . $pageWidth . " height=" . $pageHeight);
        }

        $ury = $pageHeight - ($vert_margin * 2);

        // add heading if this is first page

        $titleOpts = " encoding=" . $encoding . " embedding=" . $embedding . " textrendering=0 fontname {" . $fontnameb . "} fontsize=" . $h0fs . " leading=" . $leading . " alignment=right ";

        $TF = $p->create_textflow($heading, $titleOpts . $strokecolor);
        $TFResult = $p->fit_textflow($TF, 0, ($pageHeight - $vert_margin + 10), ($pageWidth - $horz_margin), $pageHeight - 70, " showborder=false rewind=0 ");
        $ury = $p->info_textflow($TF, "textendy") - 40;

        $TF = $p->create_textflow($metadata, $headingOpts . $strokecolor);
        $TFResult = $p->fit_textflow($TF, $horz_margin, ($vert_margin), ($pageWidth - $horz_margin), $ury, " showborder=false rewind=0 ");
        $ury = $p->info_textflow($TF, "textendy") - 20;


        // if the section has a table add it to the page
        $table_props = [];
        $table_props['cols'] = $table_cols;
        $table_props['cols_width'] = $table_cols_width;
        $table_props["content"] = $content[$data_key];
        $table_props["data_key"] = $data_key;

        placeTable($p, $pgno, $content_width, $fontname, $bodyfs, $table_props, $vert_margin, $horz_margin, $urx, $ury, $pageHeight, $pageWidth, $vert_margin);

        // Loop until all of the text is placed; create new pages

        if ($TFResult == "_boxfull") {
            addNewPage($p, $pgno, $fontname, $pageWidth, $pageHeight);
            $ury = $pageHeight - $vert_margin;
            $pgno++;
            $TFResult = $p->fit_textflow($TF, $horz_margin, $vert_margin, $pageWidth - $horz_margin, $ury, " showborder=false rewind=0 ");
            $ury = $p->info_textflow($TF, "textendy");
        }


        if ($TFResult == "_stop") {

            if ($ury < 120) {
                addNewPage($p, $pgno, $fontname, $pageWidth, $pageHeight);
                $ury = $pageHeight - ($vert_margin);
            }

            addPageNo($p, $pgno, $fontname, $pageWidth);
        }

        $p->end_page_ext("");

        $p->end_document("");

        if ($save) {
            header("Content-disposition: attachment; filename=" . $filename . "");
            readfile($tmpfile);

        } else {
            $buf = $p->get_buffer();
            $len = strlen($buf);
            header("Content-type: application/pdf");
            header("Content-Transfer-Encoding: Binary");
            header("Content-Length: $len");
            header("Content-Disposition: inline; filename='" . $filename . "'");
            print $buf;
        }
    }

}

function addNewPage($p, $pgno, $fontname, $pageWidth, $pageHeight)
{
    // add page number at footer
    addPageNo($p, $pgno, $fontname, $pageWidth);

    $p->end_page_ext("");
    $p->begin_page_ext(0, 0, "width=" . $pageWidth . " height=" . $pageHeight);
}

function addPageNo($p, $pgno, $fontname, $pageWidth)
{
    $TFO = " encoding=unicode embedding=true textrendering=0 fontname {" . $fontname . "} fontsize=9 alignment=center";
    $llx = 0;
    $urx = $pageWidth;
    $txt = "pg." . $pgno;
    $TF = $p->create_textflow($txt, $TFO);
    $result = $p->fit_textflow($TF, $llx, 6, $urx, 16, "");
}

function placeTable($p, $pgno, $content_width, $fontname, $fontsize, $table_prop, $lly, $llx, $urx, $ury, $page_height, $page_width, $vert_margin)
{
    $tbl = 0;
    $rgb = new RGBcolors();
    $cols = $table_prop['cols'];
    $cols_width = $table_prop['cols_width'];
    $data_key = $table_prop['data_key'];
    $content = $table_prop['content'];
    $table_width = $urx - $llx;
    $dy = 35;
    $rh = 35;
    $cr = $rh*.4;
    $images = [];

    for($i=0;$i < 13; $i++){
         $img = "icons/".($i+1).".png";
        $images[] = $p->load_image("auto",$img,"");
        if ( $images[$i] == 0) {
            echo("Couldn't load $img: " . $p->get_errmsg());
            exit(1);
        }
    }

    /*** loop over the table rows ***/
    $cx = $llx + $table_width * (($cols_width[0]+$cols_width[1]*.5)/100);
    /* Set the stroke color */
    $p->set_graphics_option("strokecolor={rgb 0 0.5 0.5}");

    /* Set the fill color */
    $p->set_graphics_option("fillcolor={rgb 0 0.85 0.85}");
    foreach ($content as $r => $row) {
        foreach ($cols as $c => $col) {
            $cw = ($table_width * $cols_width[$c] / 100);
            $v = $row[$col];
            $fakebold = $c == 0? " fakebold=true " : "";
            if($col == "type"){
                $ci = ($v-1) % 2;
                $cc = $rgb->getColor($ci);
                /* Draw a circle at position (x, y) with a radius of 40 */
               /* $x =  ($cx);
                $y = $ury - ($rh * $r) + $rh*.5;
                $p->set_graphics_option("fillcolor={".$cc."}");
                $p->circle($x, $y-=$dy, $cr);
                $p->fill();*/

                $optlist = "image=" . $images[$v-1] . " colwidth=" . $cw . " rowheight=20 margin=8 fitimage={fitmethod=meet ";
                $optlist.= "matchbox={borderwidth=0 offsetleft=-2 offsetright=2 " .
                    "offsetbottom=-2 offsettop=2 linecap=round linejoin=round  " .
                    "fillcolor {".$cc."} } }";

                $tbl = $p->add_table_cell($tbl, ($c + 1), ($r + 1), "", $optlist);

            }else{
                if (!is_array($v) && $col != $data_key) {
                    $tf = $p->create_textflow($v, " encoding=unicode fontname=" . $fontname . " fontsize=" . $fontsize . " leading=100% ". $fakebold);
                    $tfo = "textflow=" . $tf . " fittextflow={firstlinedist=capheight} colwidth=" . $cw . " rowheight=".$rh."  margin=6 ";
                    $tbl = $p->add_table_cell($tbl, ($c + 1), ($r + 1), "", $tfo);
                    $tf = 0;
                }
            }

        }

    }

    $optlist = " rowheightdefault=auto "
        . "stroke={{line=other linewidth=0  }}";

    /* Place the table instance */
    $result = $p->fit_table($tbl, $llx, $lly, $urx, $ury, $optlist);

    if ($result == "_boxfull") {
        addPageNo($p, $pgno, $fontname, $page_width);
        $pgno++;
        $p->end_page_ext("");
        $p->begin_page_ext(0, 0, "width=" . $page_width . " height=" . $page_height);
        $ury = ($page_height - $vert_margin);
        $result = $p->fit_table($tbl, $llx, $vert_margin, $urx, $ury, $optlist);
    }

    $tabheight = $ury - $p->info_table($tbl, "height");
    $p->delete_table($tbl, "");
    return [$pgno, $tabheight];
}

foreach ($_POST as $key => $val) {
    ${$key} = $val;
}

if (isset($format)) {
    $r = new PDFout($_POST);
    $r->generatePDF();
}