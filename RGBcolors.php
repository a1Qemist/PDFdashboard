<?php


class RGBcolors
{
    public $rgbArray;

    public function __construct($colorIndex = 0){
        $this->rgbArray  = [[174,227,154],   [138,225,249], [19,90,194], [228,181,255],[9,123,53], [177,20,120],[71,240,163], [89,32,175], [37,115,139], [252,103,186], [91,60,94], [187,84,237], [154,120,158], [250,27,252], [173,213,31]];
    }

    public function getColor($colorIndex){
        $ca = $this->rgbArray;
        $rgbitem = $ca[$colorIndex];
        $rgbout = []   ;
        forEach($rgbitem as $c){
            $rgbout[] = number_format($c/255,2);
        }
        return " rgb ".implode(" ",$rgbout)." ";
    }

}