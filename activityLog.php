<?php


class activityLog
{
    public $filename;
    public $format;
    public $report_title;
    public $author;
    public $heading;
    public $table_cols;
    public $table_cols_width;
    public $data_key;
    public $metadata;

    public function __construct($request_id, $report_type, $table_cols, $table_cols_width)
    {
        $this->request_id = $request_id;
        $this->filename = "activity_pages";
        $this->format = "table";
        $this->report_title = $report_title;
        $this->author = "Periscope";
        $this->table_cols = $table_cols; //["datetime","type","message","activities"];
        $this->table_cols_width = $table_cols_width; //"26,8,62,0";
        $this->data_key = "activities";
        $this->metadata = "Patient: Evista Gorbitano      Assessment ID: W4566FG0";
    }

    public function generateActivtyPDF()
    {
        $ca = $this->rgbArray;
        $rgbitem = $ca[$colorIndex];
        $rgbout = [];
        foreach ($rgbitem as $c) {
            $rgbout[] = number_format($c / 255, 2);
        }
        return " rgb " . implode(" ", $rgbout) . " ";
    }

}