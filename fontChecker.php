<?php


class FontChecker
{
    public $fontdir;
    public $p;

    public function __construct($p,$fontdir)
    {
        $this->fontdir = $fontdir;
        $this->p = $p;
    }

 function getFonts(){
        $fonts = array();
        $count = 0;

     $fontdir = $this->fontdir;
     $p = $this->p;

        $p->set_option("searchpath={{" .$fontdir . "}}");

        /* enumerate all fonts on the searchpath and create a UPR file */
        $p->set_option("enumeratefonts saveresources={filename={font_lister_pdflib.upr}}");

        /* Retrieve the names of all enumerated fonts */
        do{
            $fontresource = $p->get_option("FontOutline", "resourcenumber=" . ++$count);
            if ($fontresource == -1) {
                break;
            }
            $resourceentry = $p->get_string($fontresource, "");
            $fonts[] = explode(" = " , $resourceentry)[0];
        } while ($fontresource != -1);

        return ($fonts);
    }

}