<?php

$activity = (file_get_contents("data/activity.json"));
$media = (file_get_contents("data/media.json"));

$activity = json_encode($activity,JSON_PRETTY_PRINT );
$media = json_encode($media,JSON_PRETTY_PRINT);


?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <style>
        @import url(css/style.css);
    </style>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>PDF Dashboard</title>
    <script>
        var loaded_data = {
            "activity":<?php echo $activity; ?>,
            "media":<?php echo $media; ?>
        }
    </script>
</head>
<body onload="setSize()">
<div class="mb-0 border-0 card"
     style="background-image: unset; background-size: unset; background-attachment: unset; background-origin: unset; background-clip: unset; background-color: unset; background-position: unset; background-repeat: unset;">
    <div class="row">
        <div class="col" style="flex-grow: 10;"></div>
        <div class="col" style="flex-grow: 10;">
            <form method="POST" action="PDFout.php" id="report_editor" target="myIframe">
                <div class="mb-0 border-0 card">
                    <div class="p-0">

                        <div class="row">
                            <div class="col" style="flex-grow: 5;">
                                <label>Chapter</label>
                                <select id="report" name="report" class="form-control" onchange="setupDocument()">
                                    <option value="activity">Activities</option>
                                    <option value="media">Photos/Media</option>
                                    <option value="documents">Documents</option>
                                </select>
                            </div>
                            <div class="col" style="flex-grow: 5;">
                                <label for="request_id">Request ID</label>
                                <input name="request_id" id="request_id" type="text" placeholder="request id"
                                       class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <input id="send_opt" name="send_opt" type="hidden" value="0" class="form-control">
                <input id="save_opt" name="save_opt" type="hidden" value="0" class="form-control">
                <input id="filename" name="filename" type="hidden" value="mypdf" class="form-control">
                <input id="format" name="format" type="hidden" value="table" class="form-control">
                <input id="report_type" name="report_type" type="hidden" value="Activities" class="form-control">
                <input id="author" name="author" type="hidden" value="Reviewer" class="form-control">
                <input id="data_key" name="data_key" type="hidden" value="activities" class="form-control">
                <textarea id="metadata" name="metadata" type="hidden">Patient: Evista Gorbitano      Assessment ID: W4566FG0
                </textarea>

                <textarea id="json" name="json">{
  "activity": [
    {
      "id": "e4fd8174-0f41-4e7b-8727-7cf582e548c4",
      "parent_id": null,
      "date": "12/09/2021",
      "datetime": "12/09/2021 13:55:41",
      "priority": true,
      "message": "New Request received",
      "type": 11,
      "activities": null
    },
 {
      "id": "c62b6ba0-d60d-40e4-99b1-ddbf77e0b404",
      "parent_id": null,
      "date": "12/09/2021",
      "datetime": "12/09/2021 11:31:58",
      "priority": true,
      "message": "Clinician contacted and assigned the request from Virginia Premier Health Care, she indicated she was available and could actually visit the patient within the hour if they were available.",
      "type": 10,
      "activities": null
    },
    {
      "id": "c62b6ba0-d60d-40e4-99b1-ddbf77e0b404",
      "parent_id": null,
      "date": "12/09/2021",
      "datetime": "12/09/2021 11:31:58",
      "priority": true,
      "message": "Member called 12/09/2021, no answer",
      "type": 5,
      "activities": null
    },
    {
      "id": "c62b6ba0-d60d-40e4-99b1-ddbf77e0b404",
      "parent_id": null,
      "date": "12/09/2021",
      "datetime": "12/09/2021 14:31:58",
      "priority": true,
      "message": "Member called 12/09/2021, appointment scheduled 12/09/2021 07:00 - 09:00.",
      "type": 2,
      "activities": null
    },
                       {
      "id": "2f6c7fe1-869a-4eaf-83fb-d42a3ba2ccf4",
      "parent_id": null,
      "date": "12/09/2021",
      "datetime": "12/09/2021 03:29:20",
      "priority": false,
      "message": "Hey Lauren I am on a Home Assessment and I have some questions.",
      "type": 3,
      "activities": null
    },
   {
      "id": "2f6c7fe1-869a-4eaf-83fb-d42a3ba2ccf4",
      "parent_id": null,
      "date": "12/09/2021",
      "datetime": "12/09/2021 03:29:20",
      "priority": false,
      "message": "Hey Kevin, what's up.",
      "type": 3,
      "activities": null
    },
  {
      "id": "2f6c7fe1-869a-4eaf-83fb-d42a3ba2ccf4",
      "parent_id": null,
      "date": "12/09/2021",
      "datetime": "12/09/2021 03:29:20",
      "priority": false,
      "message": "This patient has a ............",
      "type": 3,
      "activities": null
    },
    {
      "id": "d27d0776-ff10-4d1f-8331-c7e2f092d91f",
      "parent_id": "2f6c7fe1-869a-4eaf-83fb-d42a3ba2ccf4",
      "date": "12/09/2021",
      "datetime": "12/09/2021 018:29:20",
      "priority": false,
      "message": "Home Assessment completed, photos uploaded",
      "type": 9,
      "activities": null
    },
    {
      "id": "2f6c7fe1-869a-4eaf-83fb-d42a3ba2ccf4",
      "parent_id": null,
      "date": "12/09/2021",
      "datetime": "12/09/2021 03:29:20",
      "priority": false,
      "message": "Reviewer notified that assessment is completed.",
      "type": 6,
      "activities": null
    },
    {
      "id": "8af1200e-206c-4827-b432-84a3c52ed7e1",
      "parent_id": null,
      "date": "12/09/2021",
      "datetime": "12/09/2021 03:29:20",
      "priority": false,
      "message": "Reviewer begins to write the Narrative Report",
      "type": 7,
      "activities": null
    },
    {
      "id": "ccf416ea-aa4e-43bf-be72-7051b857b320",
      "parent_id": null,
      "date": "12/09/2021",
      "datetime": "12/09/2021 03:29:20",
      "priority": false,
      "message": "Review completes the Narrative Report and sends to VPHC",
      "type": 13,
      "activities": null
    },
                     {
      "id": "ccf416ea-aa4e-43bf-be72-7051b857b320",
      "parent_id": null,
      "date": "12/09/2021",
      "datetime": "12/09/2021 03:29:20",
      "priority": false,
      "message": "Request is designated as Completed!",
      "type": 12,
      "activities": null
    }
  ]
}
</textarea>

            </form>
        </div>
        <div class="col justify-content-center" style="flex-grow: 10;">
            <div class="row" style="height:50px; "></div>
            <div class="row btn-group d-flex" role="group">
                <button class="btn btn-primary btn-large submitbtn active" onclick="report_editor.submit()">Preview
                    PDF
                </button>
                <button class="btn btn-primary btn-large submitbtn" onclick="savePDF()">Save PDF</button>
            </div>
            <iframe src="PDFout.php" border="0" name="myIframe" id="myIframe"></iframe>
        </div>
        <div class="col" style="flex-grow: 10;">
        </div>
    </div>
</body>
<script src="js/script.js" charset="utf-8"></script>
</html>
