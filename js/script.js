let t = document.getElementsByName("template")[0] //.value
let ri = document.getElementById("report_editor")//.value
let r = document.getElementsByName("report")[0]//.value
let f = document.getElementsByName("myIframe")[0]//.src = r;
let e = document.getElementsByName("json")[0]//.value = "x";
let k = document.getElementsByName("data_key")[0]//.value = "x";

const class_objects = {"activity":"PDFactivity.php","media":"PDFmedia.php"}

function setSize() {
    let h = window.innerHeight
    let e = document.getElementById("json")
    let i = document.getElementById("myIframe")
    i.height =  (h - 200) +"px !important"
    e.rows = (h-200)/23
    i.width =  800+"px !important"
    e.cols = 80

    setupDocument()
}

function sendPDF() {
    document.getElementById("send_opt").value = 1
    document.getElementById("save_opt").value = 0
    report_editor.submit()
    document.getElementById("send_opt").value = 0
}

function savePDF() {
    document.getElementById("save_opt").value = 1
    document.getElementById("send_opt").value = 0
    report_editor.submit()
    document.getElementById("save_opt").value = 0
}
function setupDocument(){
    let s = r.value
    ri.action = class_objects[s]
    f.src = class_objects[s]
    k.value = s
    e.value = loaded_data[s]
    report_editor.submit()
}
function previewData(){
    document.getElementsByName("template")[0].value
}

